package com.ee.assessment;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

public class SequenceGeneratorTest {

	@Test
	public void test_range_by_input() {
		
		SequenceGenerator generator = new SequenceGenerator();
		
		List<String> sequence = generator.getSequence(1, 0);
		assertEquals(0, sequence.size());
		
		sequence = generator.getSequence(-1, -2);
		assertEquals(0, sequence.size());
		
		sequence = generator.getSequence(0, 0);
		assertEquals(1, sequence.size());
		
		sequence = generator.getSequence(1, 1);
		assertEquals(1, sequence.size());
		
		sequence = generator.getSequence(1, 100);
		assertEquals(100, sequence.size());
		
		
		sequence = generator.getSequence(1, 1000);
		assertEquals(1000, sequence.size());
		
		
		sequence = generator.getSequence(1, 10000);
		assertEquals(10000, sequence.size());
		
		
		sequence = generator.getSequence(1, 100000);
		assertEquals(100000, sequence.size());
	}
	
	
	@Test
	public void test_fizz_occurrences() {
		
		SequenceGenerator generator = new SequenceGenerator();
		
		List<String> sequence = generator.getSequence(1, 10);
		
		assertEquals("fizz", sequence.get(2));
		
		assertEquals("fizz", sequence.get(5));
		
		assertEquals("fizz", sequence.get(8));
	}

	@Test
	public void should_got_five_fizz_in_1_20_range() {
		
		SequenceGenerator generator = new SequenceGenerator();
		
		List<String> sequence = generator.getSequence(1, 20);
		
		int fizzCount = 0;
		for(String entry: sequence){
			if(entry.equals("fizz")) fizzCount ++;
		}
		
		assertEquals(5, fizzCount);
	}
	
	
	@Test
	public void test_buzz_occurrences() {
		
		SequenceGenerator generator = new SequenceGenerator();
		
		List<String> sequence = generator.getSequence(1, 20);
		
		assertEquals("buzz", sequence.get(4));
		
		assertEquals("buzz", sequence.get(9));
		
		assertEquals("buzz", sequence.get(19));
	}
	
	
	@Test
	public void should_got_three_buzz_in_1_20_range() {
		
		SequenceGenerator generator = new SequenceGenerator();
		
		List<String> sequence = generator.getSequence(1, 20);
		
		int buzzCount = 0;
		for(String entry: sequence){
			if(entry.equals("buzz")) buzzCount ++;
		}
		
		assertEquals(3, buzzCount);
	}
	
	
	@Test
	public void should_got_one_fizzbuzz_in_1_20_range() {
		
		SequenceGenerator generator = new SequenceGenerator();
		
		List<String> sequence = generator.getSequence(1, 20);
		
		int buzzCount = 0;
		for(String entry: sequence){
			if(entry.equals("fizzbuzz")) buzzCount ++;
		}
		
		assertEquals(1, buzzCount);
	}
	
	
	@Test
	public void test_any_range(){
		
		SequenceGenerator generator = new SequenceGenerator();
		
		for(int i = 10; i<1000; i = i +10){
			
			List<String> sequence = generator.getSequence(1, i);
			
			int threeAndFiveMultiples = getTotalMultipleof15ForRange(i);
			
			int threeMultiples = getTotalMultipleof3ForRange(i);
			
			int fiveMultiples = getTotalMultipleof5ForRange(i);
			
			int expectedTotalFizz = threeMultiples - threeAndFiveMultiples;
			int expectedTotalBuzz = fiveMultiples - threeAndFiveMultiples;

			
			int fizzCount = count("fizz", sequence);
			
			assertEquals(expectedTotalFizz, fizzCount);
			
			
			int buzzCount = count("buzz", sequence);
			
			assertEquals(expectedTotalBuzz, buzzCount);
			
			int fizzBuzzCount = count("fizzbuzz", sequence);
			
			assertEquals(threeAndFiveMultiples, fizzBuzzCount);
		}
	}
	
	
	private int count(String type, List<String> sequence) {
		
		int count = 0;
		
		for(String entry: sequence){
			if(entry.equals(type)) count ++;
		}
		
		return count;
	}


	public int getTotalMultipleof3ForRange(int range){
		return range/3;
	}
	
	
	public int getTotalMultipleof5ForRange(int range){
		return range/5;
	}

	
	public int getTotalMultipleof15ForRange(int range){
		return range/15;
	}
	
}

package com.ee.assessment;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

public class SequenceGeneratorTest {

	@Test
	public void test_range_by_input() {
		
		SequenceGenerator generator = new SequenceGenerator();
		
		List<String> sequence = generator.getSequence(1, 0);
		assertEquals(0, sequence.size());
		
		sequence = generator.getSequence(-1, -2);
		assertEquals(0, sequence.size());
		
		sequence = generator.getSequence(0, 0);
		assertEquals(1, sequence.size());
		
		sequence = generator.getSequence(1, 1);
		assertEquals(1, sequence.size());
		
		sequence = generator.getSequence(1, 100);
		assertEquals(100, sequence.size());
		
		
		sequence = generator.getSequence(1, 1000);
		assertEquals(1000, sequence.size());
		
		
		sequence = generator.getSequence(1, 10000);
		assertEquals(10000, sequence.size());
		
		
		sequence = generator.getSequence(1, 100000);
		assertEquals(100000, sequence.size());
	}
	
	
	@Test
	public void test_fizz_occurrences() {
		
		SequenceGenerator generator = new SequenceGenerator();
		
		List<String> sequence = generator.getSequence(1, 10);
		
		assertEquals("fizz", sequence.get(5));
		
		assertEquals("fizz", sequence.get(8));
	}

	@Test
	public void should_got_four_fizz_in_1_20_range() {
		
		SequenceGenerator generator = new SequenceGenerator();
		
		List<String> sequence = generator.getSequence(1, 20);
		
		int fizzCount = 0;
		for(String entry: sequence){
			if(entry.equals("fizz")) fizzCount ++;
		}
		
		assertEquals(4, fizzCount);
	}
	
	
	@Test
	public void test_buzz_occurrences() {
		
		SequenceGenerator generator = new SequenceGenerator();
		
		List<String> sequence = generator.getSequence(1, 20);
		
		assertEquals("buzz", sequence.get(4));
		
		assertEquals("buzz", sequence.get(9));
		
		assertEquals("buzz", sequence.get(19));
	}
	
	
	@Test
	public void should_got_three_buzz_in_1_20_range() {
		
		SequenceGenerator generator = new SequenceGenerator();
		
		List<String> sequence = generator.getSequence(1, 20);
		
		int buzzCount = 0;
		for(String entry: sequence){
			if(entry.equals("buzz")) buzzCount ++;
		}
		
		assertEquals(3, buzzCount);
	}
	
	
	@Test
	public void should_got_one_fizzbuzz_in_1_20_range() {
		
		SequenceGenerator generator = new SequenceGenerator();
		
		List<String> sequence = generator.getSequence(1, 20);
		
		int buzzCount = 0;
		for(String entry: sequence){
			if(entry.equals("fizzbuzz")) buzzCount ++;
		}
		
		assertEquals(1, buzzCount);
	}
	
	
	
	@Test
	public void test_lucky_occurrences_in_range_1_20() {
		
		SequenceGenerator generator = new SequenceGenerator();
		
		List<String> sequence = generator.getSequence(1, 20);
		
		assertEquals("lucky", sequence.get(2));
		
		assertEquals("lucky", sequence.get(12));
	}
	
	
	@Test
	public void test_any_range(){
		
		SequenceGenerator generator = new SequenceGenerator();
		
		for(int i = 10; i<1000; i = i +10){
			
			List<String> sequence = generator.getSequence(1, i);
			
			int expetedTotalLucky = getTotalOfNumsContainingAThreeDigitForRange(i);
			
			int totalLuckyGot = 0;
			
			for(String entry: sequence){
				if(entry.equals("lucky")) totalLuckyGot ++;
			}
						
			assertEquals(expetedTotalLucky, totalLuckyGot);			
		}
	}

	
	@Test
	public void test_report_data()
	{
		SequenceGenerator generator = new SequenceGenerator();
		
		for(int i = 10; i<1000; i = i +10)
		{
			List<String> sequence = generator.getSequence(1, i);
			
			int expetedTotalLucky = getTotalOfNumsContainingAThreeDigitForRange(i);
			
			assertEquals(expetedTotalLucky, generator.getCountLucky());

		}
	}

	private int getTotalOfNumsContainingAThreeDigitForRange(int range) {
		int count = 0;
		for(int i = 1; i<= range; i++){
			
			int currDig = 0;
			int curr = i;
			while(curr != 0){
				
				currDig = curr % 10;
				curr = curr/10;
				
				if(currDig == 3) {
					count++;
					break;
				}
			}
		}
		return count;
	}

}

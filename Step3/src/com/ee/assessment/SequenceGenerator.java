package com.ee.assessment;


import java.util.ArrayList;
import java.util.List;

public class SequenceGenerator {
	
	private int countNumber = 0;
	private int countLucky = 0;
	private int countFizz = 0;
	private int countBuzz = 0;
	private int countFizzBuzz = 0;
	
	public List<String> getSequence(int start, int end)
	{
		List<String> result = new ArrayList<>();
	
		//clear counters
		this.countNumber = 0;
		this.countLucky = 0;
		this.countFizz = 0;
		this.countBuzz = 0;
		this.countFizzBuzz = 0;
		
		//Accept only positive integers for range
		if(start < 0) return result;
		if(end < 0) return result;
		
		if(end < start) return result;
		
		for(int i = start; i<= end; i++)
		{
			String entry = transform(i);
			result.add(entry);
			
			switch(entry){
			case "lucky" :
				this.countLucky ++;
				break;
			case "fizz" :
				this.countFizz ++;
				break;
			case "buzz" :
				this.countBuzz ++;
				break;
			case "fizzbuzz" :
				this.countFizzBuzz ++;
				break;
			default:
				this.countNumber ++;
			}
		}
		
		return result;
	}

	
	public int getCountNumber() {
		return countNumber;
	}


	public int getCountLucky() {
		return countLucky;
	}


	public int getCountFizz() {
		return countFizz;
	}


	public int getCountBuzz() {
		return countBuzz;
	}


	public int getCountFizzBuzz() {
		return countFizzBuzz;
	}


	private String transform(int number) 
	{
		if(containsThree(number)) return "lucky";
		
		if(isMultipleOfThree(number)) 
		{
			//return fizz only if number is not multiple of five too 
			if(!isMultipleOfFive(number)) return "fizz";
			
			return "fizzbuzz";
		}
		
		if(isMultipleOfFive(number)) 
		{
			if(!isMultipleOfThree(number)) return "buzz";
			
			return "fizzbuzz";
		}
		
		return new Integer(number).toString();
	}
	
	
	private boolean containsThree(int curr) {
		
		int currDig = 0;
		while(curr != 0){
			currDig = curr % 10;
			curr = curr/10;
			
			if(currDig == 3) return true;
		}
		return false;
	}
	
	
	private boolean isMultipleOfThree(int number) {
		
		return (number % 3) == 0;
	}

	

	private boolean isMultipleOfFive(int number) {
		
		return (number % 5) == 0;
	}
}

package com.ee.assessment;

import java.util.List;

public class Main {

	public static void main(String[] args) {
		
		if(args.length < 2){
			System.out.println("Please inform the range for the sequence. Ex: Main 1 20");
			System.exit(0);
		}
		
		int start = 0;
		int end = 0;
		
		try{
			start = Integer.parseInt(args[0]);
			end = Integer.parseInt(args[1]);
		}
		catch(NumberFormatException e){
			System.out.println("Invalid number. Please enter integer numbers. Ex: Main 1 27");
			System.exit(0);
		}

		SequenceGenerator generator = new SequenceGenerator();
		List<String> sequence = generator.getSequence(start, end);
		printSequence(sequence);
		printReport(generator);
	}

	
	private static void printSequence(List<String> sequence) {

		for(String entry: sequence)
		{
			System.out.print(entry);
			System.out.print(" ");
		}
	}
	
	

	private static void printReport(SequenceGenerator generator) 
	{
		System.out.println("\nfizz: " + generator.getCountFizz());
		System.out.println("buzz: " + generator.getCountBuzz());
		System.out.println("fizzbuzz: " + generator.getCountFizzBuzz());
		System.out.println("lucky: " + generator.getCountLucky());
		System.out.println("integer: " + generator.getCountNumber());		
	}
}

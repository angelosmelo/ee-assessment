# README #



### Dependencies ###

* Ant 1.9.8+
* JUnit 4

### Compiling and running ###

Each step is an individual project.

1. Choose a step folder.
1. Compile the project.
1. Execute the java Main class informing the range (start, end)

For instance, executing the Step1 project with a range of 1-20:

* cd Step1
* ant build
* java -cp build/main com.ee.assessment.Main 1 20 

### Running the unit tests ###

1. Choose a step folder.
1. Execute ant test

For instance, testing the Step1:

* cd Step1
* ant test


### Who do I talk to? ###

* For any doubt, contact-me by email:
angelo.s.melo@gmail.com
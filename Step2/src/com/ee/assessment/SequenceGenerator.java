package com.ee.assessment;


import java.util.ArrayList;
import java.util.List;

public class SequenceGenerator {
	
	public List<String> getSequence(int start, int end)
	{
		List<String> result = new ArrayList<>();
		
		//Accept only positive integers for range
		if(start < 0) return result;
		if(end < 0) return result;
		
		if(end < start) return result;
		
		for(int i = start; i<= end; i++)
		{
			result.add(transform(i));
		}
		
		return result;
	}

	
	private String transform(int number) 
	{
		if(containsThree(number)) return "lucky";
		
		if(isMultipleOfThree(number)) 
		{
			//return fizz only if number is not multiple of five too 
			if(!isMultipleOfFive(number)) return "fizz";
			
			return "fizzbuzz";
		}
		
		if(isMultipleOfFive(number)) 
		{
			if(!isMultipleOfThree(number)) return "buzz";
			
			return "fizzbuzz";
		}
		
		return new Integer(number).toString();
	}
	
	
	private boolean containsThree(int curr) {
		
		int currDig = 0;
		while(curr != 0){
			currDig = curr % 10;
			curr = curr/10;
			
			if(currDig == 3) return true;
		}
		return false;
	}
	
	
	private boolean isMultipleOfThree(int number) {
		
		return (number % 3) == 0;
	}

	

	private boolean isMultipleOfFive(int number) {
		
		return (number % 5) == 0;
	}
}
